﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeInfoWithLayerArch.BLL;
using EmployeeInfoWithLayerArch.DAL.DAO;

namespace EmployeeInfoWithLayerArch.UI
{
    public partial class EntryUI : Form
    {
        private Designation aDesignation = new Designation();
        private DesignationManager aDesignationManager = new DesignationManager();
        private EmployeeManager employeeManager = new EmployeeManager();
        private int employeeID;

        public EntryUI()
        {
            InitializeComponent();
            LoadAllDesignation();
        }


        public EntryUI(Employee employee) : this()
        {
            saveButton.Text = @"Update";
            FillFieldsWith(employee);
            employeeID = employee.ID;
        }


        private void FillFieldsWith(Employee employee)
        {
            nameTextBox.Text = employee.Name;
            addressTextBox.Text = employee.Address;
            emailTextBox.Text = employee.Email;
            designationComboBox.SelectedValue = employee.aDesignation.ID;

        }


        private void addMoreButton_Click(object sender, EventArgs e)
        {
            AddDesignationUI newFrom = new AddDesignationUI();
            newFrom.ShowDialog();
            LoadAllDesignation();
            Designation lastAddedDesignation = newFrom.GetLastAddedDsDesignationByThisUI();
            if (lastAddedDesignation != null)
            {
                designationComboBox.Text = lastAddedDesignation.Title;
            }

        }

        public void LoadAllDesignation()
        {

            designationComboBox.DataSource = aDesignationManager.GetAllDesignations();

            designationComboBox.DisplayMember = "Title";

            designationComboBox.ValueMember = "ID";
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            Employee anEmployee = new Employee();
            anEmployee.ID = employeeID;
            anEmployee.Name = nameTextBox.Text;
            anEmployee.Email = emailTextBox.Text;
            anEmployee.Address = addressTextBox.Text;

            anEmployee.aDesignation = (Designation) designationComboBox.SelectedItem;

            if (nameTextBox.Text != null || emailTextBox.Text != null || addressTextBox.Text != null ||
                designationComboBox.Text != null)
            {
                if (saveButton.Text != @"Update")

                {
                    string message = "";
                    message = employeeManager.Save(anEmployee);
                    MessageBox.Show(message, @"Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearEmployee();
                }
                else
                {
                    string message = "";
                    message = employeeManager.Update(anEmployee);
                    MessageBox.Show(message, @"Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearEmployee();
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show(@"Please fill-up the Employee's information properly", @"Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

        }


        private void ClearEmployee()
        {
            nameTextBox.Text = "";
            emailTextBox.Text = "";
            addressTextBox.Text = "";
            designationComboBox.Text = "";

        }
    }
}
