﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeInfoWithLayerArch.UI
{
    public partial class MainUI : Form
    {
        public MainUI()
        {
            InitializeComponent();
           
        }


        private void addButton_Click(object sender, EventArgs e)
        {
            EntryUI entryUi=new EntryUI();
            entryUi.Show();
        }

        private void findEditButton_Click(object sender, EventArgs e)
        {
            FindemployeeUI findemployeeUi=new FindemployeeUI();
            findemployeeUi.Show();
        }

    }
}
