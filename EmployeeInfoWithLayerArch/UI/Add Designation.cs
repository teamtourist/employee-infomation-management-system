﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeInfoWithLayerArch.BLL;
using EmployeeInfoWithLayerArch.DAL.DAO;

namespace EmployeeInfoWithLayerArch.UI
{
    public partial class AddDesignationUI : Form
    {
        public AddDesignationUI()
        {
            InitializeComponent();

        }

private   Designation newDesignation = new Designation();
        DesignationManager aDesignationManager = new DesignationManager();
        private void saveButton_Click(object sender, EventArgs e)
        {
            string message;
            newDesignation.Code = codeTextBox.Text;
            newDesignation.Title = titleTextBox.Text;
            if (aDesignationManager.Save(newDesignation ,out message))
            {
                MessageBox.Show(message, "Succeed");
                this.Close();
            }
            else
            {
                
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        public Designation GetLastAddedDsDesignationByThisUI()
        {
            return newDesignation;
        }
        

        
       
    }
}
