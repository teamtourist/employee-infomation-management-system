﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeInfoWithLayerArch.DAL.DAO;
using EmployeeInfoWithLayerArch.DAL.Gateway;

namespace EmployeeInfoWithLayerArch.BLL
{
    public class DesignationManager
    {
        private Designation employeeDesignation = new Designation();
        private DesignationGateway newDesignationGateway = new DesignationGateway();

        public bool Save(Designation aDesignation, out string message)
        {
            if (aDesignation.Code == null)
            {
                message = "Code field is empty";
                return false;
            }
            else if (aDesignation.Title == null)
            {
                message = "Title is empty";
                return false;
            }
            else if (newDesignationGateway.HasDesignationCode(aDesignation.Code))
            {
                message = "Code already exist";
                return false;
            }
            else if (newDesignationGateway.HasDesignationTitle(aDesignation.Title))
            {
                message = "Title already exist";
                return false;
            }
            else
            {
                newDesignationGateway.Save(aDesignation);
                message = "save successfully";
                return true;
            }



        }

        public List<Designation> GetAllDesignations()
        {
            return newDesignationGateway.GetAll();
        }
    }
}
