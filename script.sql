USE [master]
GO
/****** Object:  Database [EmployeeInfoDB]    Script Date: 27/05/2015 04:10:31 ******/
CREATE DATABASE [EmployeeInfoDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EmployeeInfoDB', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\EmployeeInfoDB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EmployeeInfoDB_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\EmployeeInfoDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EmployeeInfoDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EmployeeInfoDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EmployeeInfoDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [EmployeeInfoDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EmployeeInfoDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EmployeeInfoDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EmployeeInfoDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EmployeeInfoDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [EmployeeInfoDB] SET  MULTI_USER 
GO
ALTER DATABASE [EmployeeInfoDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EmployeeInfoDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EmployeeInfoDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EmployeeInfoDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [EmployeeInfoDB]
GO
/****** Object:  Table [dbo].[Designation_tbl]    Script Date: 27/05/2015 04:10:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Designation_tbl](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Title] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Designation_tbl] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employee_tbl]    Script Date: 27/05/2015 04:10:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee_tbl](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Address] [varchar](500) NULL,
	[DesignationID] [int] NOT NULL,
 CONSTRAINT [PK_Employee_tbl] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Designation_tbl] ON 

INSERT [dbo].[Designation_tbl] ([ID], [Code], [Title]) VALUES (1, N'01', N'CEO')
INSERT [dbo].[Designation_tbl] ([ID], [Code], [Title]) VALUES (2, N'09', N'Manager')
INSERT [dbo].[Designation_tbl] ([ID], [Code], [Title]) VALUES (3, N'45', N'Assistant')
INSERT [dbo].[Designation_tbl] ([ID], [Code], [Title]) VALUES (4, N'89', N'Staff')
INSERT [dbo].[Designation_tbl] ([ID], [Code], [Title]) VALUES (5, N'14', N'Accountant')
INSERT [dbo].[Designation_tbl] ([ID], [Code], [Title]) VALUES (6, N'02', N'Chairman')
SET IDENTITY_INSERT [dbo].[Designation_tbl] OFF
SET IDENTITY_INSERT [dbo].[Employee_tbl] ON 

INSERT [dbo].[Employee_tbl] ([ID], [Name], [Email], [Address], [DesignationID]) VALUES (1, N'Abdullah', N'abu@gmail.com', N'Baandarban,CTG', 1)
INSERT [dbo].[Employee_tbl] ([ID], [Name], [Email], [Address], [DesignationID]) VALUES (2, N'Johirul Haque', N'johir@mail.com', N'Banai, Dhaka', 3)
INSERT [dbo].[Employee_tbl] ([ID], [Name], [Email], [Address], [DesignationID]) VALUES (3, N'Haque  Johirul ', N'Hjohir@mail.com', N'Dhaka', 4)
INSERT [dbo].[Employee_tbl] ([ID], [Name], [Email], [Address], [DesignationID]) VALUES (4, N'Bodrul', N'sky@mail.com', N'Ctg', 5)
INSERT [dbo].[Employee_tbl] ([ID], [Name], [Email], [Address], [DesignationID]) VALUES (5, N'Akter', N'akter@gmail.com', N'Dhaka', 6)
SET IDENTITY_INSERT [dbo].[Employee_tbl] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Designation_tbl]    Script Date: 27/05/2015 04:10:31 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Designation_tbl] ON [dbo].[Designation_tbl]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Designation_tbl_1]    Script Date: 27/05/2015 04:10:31 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Designation_tbl_1] ON [dbo].[Designation_tbl]
(
	[Title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Employee_tbl]    Script Date: 27/05/2015 04:10:31 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Employee_tbl] ON [dbo].[Employee_tbl]
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Employee_tbl]  WITH CHECK ADD  CONSTRAINT [FK_Employee_tbl_Designation_tbl] FOREIGN KEY([DesignationID])
REFERENCES [dbo].[Designation_tbl] ([ID])
GO
ALTER TABLE [dbo].[Employee_tbl] CHECK CONSTRAINT [FK_Employee_tbl_Designation_tbl]
GO
ALTER TABLE [dbo].[Employee_tbl]  WITH CHECK ADD  CONSTRAINT [FK_Employee_tbl_Employee_tbl] FOREIGN KEY([ID])
REFERENCES [dbo].[Employee_tbl] ([ID])
GO
ALTER TABLE [dbo].[Employee_tbl] CHECK CONSTRAINT [FK_Employee_tbl_Employee_tbl]
GO
USE [master]
GO
ALTER DATABASE [EmployeeInfoDB] SET  READ_WRITE 
GO
